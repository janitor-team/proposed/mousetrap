/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  mousetrap.cpp -  The main game's code.
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */




#ifndef _MOUSETRAP_H
#define _MOUSETRAP_H 1




/**
 * Screen Sizes
 */
#define SCREEN_WIDTH  640
#define SCREEN_HEIGHT 480

/**
 * Sprite size(s)
 */
#define SPRITE_SIZE    32
#define CIRCLE_SIZE     8




#endif  /* _MOUSETRAP_H */
