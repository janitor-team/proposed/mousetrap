/* -*-mode: C++; style: K&R; c-basic-offset: 4 ; -*- */

/*
 *  highscore.cpp -  A singleton for holding the game's highscore
 *
 *  mousetrap - A simple arcade game
 *
 * Homepage:
 *  http://www.steve.org.uk/Software/mousetrap
 *
 * Author:
 *  Steve Kemp <steve@steve.org.uk>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA
 *
 *  Steve Kemp
 *  ---
 *  http://www.steve.org.uk/
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


#include "highscore.h"


/*
 * Set our instance pointer to NULL - this will ensure the
 * singleton accessor works as expected.
 */
CHighScore *CHighScore::m_instance = NULL;



/*
 * Probably needs to be relaxed for *BSD.
 */
#if !defined(__GLIBC__)
# error "You'll need to do something with the highscore functionality"
#endif


/**
 * Constructor.
 *
 * This isn't public as we're a singleton.
 */
CHighScore::CHighScore()
{
    /*
     * We don't have a valid cached score.
     */
    m_valid = 0;
}

/**
 * Gain access to the one and only instance of the highscore object.
 * @returns The Highscore Singleton.
 */
/* static */ CHighScore *CHighScore::getInstance()
{
    if ( !m_instance )
    {
	m_instance = new CHighScore();
    }

    return( m_instance );
}


/**
 * Read and return the current highscore.
 * @return The current highscore.
 */
int CHighScore::readHighScore()
{

    /*
     * If we have a cached value - return it.
     */
    if ( m_valid )
	return( m_highscore );


    /*
     * Set to zero in case of error.
     */
    m_highscore = 0;

    char f_path[1025];
    snprintf(f_path, sizeof(f_path), "%s/.mousetrap",getenv( "HOME" ));

    /*
     * Load file if it exists.
     */
    int i = access ( f_path, F_OK );
    if ( i == 0 )
    {
	char line[1025];
	FILE *f = NULL;

	f = fopen( f_path, "r" );
	if ( f == NULL )
	    return 0;

	while(fgets( line, sizeof(line)-1, f) != NULL)
	{
	    sscanf( line, "%d", &m_highscore );
	}
	fclose( f );
    }

    /*
     * We have a valid read - cache it.
     */
    m_valid = 1;

    return( m_highscore );
}


/**
 * Save away the best score.
 */
void CHighScore::saveHighScore( int score )
{
    char f_path[1025];
    snprintf(f_path, sizeof(f_path), "%s/.mousetrap",getenv( "HOME" ));

    FILE *f = fopen( f_path, "w" );
    fprintf( f, "%d\n", score );
    fclose( f );

    /*
     * Cache the value.
     */
    m_valid     = 1;
    m_highscore = score;
}


